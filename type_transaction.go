package main

import (
	"encoding/json"
	"fmt"
	"math/big"
	"strings"
	"time"
	"unsafe"
)

//struct for tranzaction
type T struct {
	From     string
	To       string
	Gas      int
	GasPrice *big.Int
	Value    *big.Int
	Data     string
	Nonce    int
}

// MarshalJSON implements the json.Unmarshaler interface.
func (t T) MarshalJSON() ([]byte, error) {
	params := map[string]interface{}{
		"from": t.From,
	}
	if t.To != "" {
		params["to"] = t.To
	}
	if t.Gas > 0 {
		params["gas"] = IntToHex(t.Gas)
	}
	if t.GasPrice != nil {
		params["gasPrice"] = BigToHex(*t.GasPrice)
	}
	if t.Value != nil {
		params["value"] = BigToHex(*t.Value)
	}
	if t.Data != "" {
		params["data"] = t.Data
	}
	if t.Nonce > 0 {
		params["nonce"] = IntToHex(t.Nonce)
	}

	return json.Marshal(params)
}

// Transaction - transaction object
type Transaction struct {
	Hash             string
	Nonce            int
	BlockHash        string
	BlockNumber      *int
	TransactionIndex *int
	From             string
	To               string
	Value            big.Int
	Gas              int
	GasPrice         big.Int
	Input            string
	Date             time.Time
	Confirmations    int
}

//proxy struct
type proxyTransaction struct {
	Hash             string  `json:"hash"`
	Nonce            hexInt  `json:"nonce"`
	BlockHash        string  `json:"blockHash"`
	BlockNumber      *hexInt `json:"blockNumber"`
	TransactionIndex *hexInt `json:"transactionIndex"`
	From             string  `json:"from"`
	To               string  `json:"to"`
	Value            hexBig  `json:"value"`
	Gas              hexInt  `json:"gas"`
	GasPrice         hexBig  `json:"gasPrice"`
	Input            string  `json:"input"`
	Date             time.Time
	Confirmations    int
}

// function for json.Unmarshaler interface.
func (t *Transaction) UnmarshalJSON(data []byte) error {
	proxy := new(proxyTransaction)
	if err := json.Unmarshal(data, proxy); err != nil {
		return err
	}

	*t = *(*Transaction)(unsafe.Pointer(proxy))

	return nil
}

func transTx(tx *Transaction) []interface{} {
	txStrs := strings.Split(txStr(tx), ",")
	txObjs := make([]interface{}, len(txStrs))
	for i, txStr := range txStrs {
		txObjs[i] = txStr
	}
	return txObjs
}

func txStr(t *Transaction) string {
	var str string
	str = fmt.Sprintf(`%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v`, t.Hash, t.Nonce, t.BlockHash, *t.BlockNumber, *t.TransactionIndex, t.From, t.To, BigToHex(t.Value), BigToHex(t.GasPrice), t.Gas, t.Input, t.Date, t.Confirmations)
	return str
}
