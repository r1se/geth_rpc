package main

import (
	"database/sql"
	"log"
	"net"
)
var db *sql.DB
const (
	CONN_HOST = "localhost"
	CONN_PORT = "3333"
	CONN_TYPE = "tcp"

	URL_GETH = "http://127.0.0.1:8545"

	DB_IP = "127.0.0.1"
	DP_PORT = "5432"
	DB_USER = "postgres"
	DB_PASS = ""
	DB_NAME = "test1"
)

func main() {
	//var host, port, user, pass string
	db = NewDB(DB_IP, DP_PORT, DB_USER, DB_PASS, DB_NAME)

	// make new client for RPC
	client := New(db, URL_GETH)

	// start routine for user update
	go UpdateAccs(db, client)

	//Get event from filter routine and channels
	//blockid - get block and send to BlockDetail
	//get pending tx and send to TransactionDetail
	blockIdChan := make(chan []string, 100)
	TsHashChan := make(chan []string, 100)
	go GetEvents(client, blockIdChan, TsHashChan)

	//Get details from transaction by hash and send to insert channel(also update user)
	TsChan := make(chan []Transaction, 100)
	go TransactionDetail(client, TsHashChan, TsChan)

	//Get details from block by hash and send bunch of transaction to TransactionDetail
	go BlockDetail(client, blockIdChan, TsHashChan)

	//Listen forever TransactionDetail for insert to DB
	go func() {
		for {
			select {
			case tx := <-TsChan:
				go InsertTransactions(db, tx)
			}
		}
	}()

	//Start listen tcp server
	l, err := net.Listen(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
	if err != nil {
		log.Fatalln("Error listening:", err.Error())
	}
	defer l.Close()

	log.Println("Listening on " + CONN_HOST + ":" + CONN_PORT)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			log.Fatalln("Error accepting: ", err.Error())
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn, client)
	}
}
