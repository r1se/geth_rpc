package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

func GetEvents(RpcClient *clientRPC, blockIdChan chan<- []string, TsHashChan chan<- []string) {
	NewBlockFilterId, err := RpcClient.NewBlockFilter()
	if err != nil {
		log.Println("Error logs filter: " + err.Error())
	}
	NewBTXFilterId, err := RpcClient.NewPendingTransactionFilter()
	if err != nil {
		log.Println("Error logs filter: " + err.Error())
	}
	timer := time.NewTimer(time.Second * 0)
	for {
		select {
		case <-timer.C:

			logs, err := RpcClient.GetFilterChanges(NewBlockFilterId)
			if err != nil {
				log.Println("Error logs filter: " + err.Error())
			} else {
				if len(logs) > 0 {
					blockIdChan <- logs
				}

			}

			txhash, err := RpcClient.GetFilterChanges(NewBTXFilterId)
			if err != nil {
				log.Println("Error logs filter: " + err.Error())
			} else {
				if len(txhash) > 0 {
					TsHashChan <- txhash
				}
			}
			timer.Reset(time.Second * 2)
		}
	}
}

func BlockDetail(client *clientRPC, blockIdChan <-chan []string, TsHashChan chan<- []string) {

	for {
		select {
		case blockIds := <-blockIdChan:
			for _, blockId := range blockIds {
				block, err := client.GetBlockByHash(blockId, true)
				if err != nil {
					log.Println("Error GetBlockByHash" + err.Error())
					return
				}
				hashslice := []string{}
				for _, TzHash := range block.Transactions {
					hashslice = append(hashslice, TzHash.Hash)
				}
				TsHashChan <- hashslice
			}
		}
	}
}

func TransactionDetail(client *clientRPC, TsHashChan <-chan []string, TsChan chan<- []Transaction) {

	for {
		select {
		case txHashs := <-TsHashChan:
			sliceTrans := []Transaction{}
			for _, hash := range txHashs {
				//get block number
				blknumb, err := client.GetBlockNumber()
				if err != nil {
					log.Println("Error GetBlock number" + err.Error())
				}
				//get transaction
				tx, err := client.GetTransactionByHash(hash)
				if err != nil {
					log.Println("Error Get Transaction by Hash" + err.Error())
				}
				if tx == nil {
					log.Println("Block " + string(blknumb) + " dont have transaction Get Transaction by Hash")
					continue
				}

				confirmations := 0
				//compare block number and hash
				if tx.BlockNumber != nil {
					confirmations = blknumb - *tx.BlockNumber
				} else {
					tx.BlockNumber = new(int)
				}
				tx.Confirmations = confirmations

				t := time.Now()
				tx.Date = t
				if confirmations < 7 {
					sliceTrans = append(sliceTrans, *tx)
				}
			}
			go UpdateAccs(db, client)
			TsChan <- sliceTrans
		}
	}
}

// Get All accounts and balance
func UpdateAccs(db *sql.DB, RpcClient *clientRPC) {

	Allaccounts, err := RpcClient.Accounts()
	if err != nil {
		log.Println("Error update accounts RPC Accounts: " + err.Error())
	}

	for _, acc := range Allaccounts {

		balance, err := RpcClient.GetBalance(acc, "latest")
		if err != nil {
			log.Println("Error update accounts RPC GetBalance: " + err.Error())
		}
		RpcClient.mapmutex.Lock()
		if _, ok := RpcClient.accounts[acc]; !ok {
			bal := toEth(balance)
			RpcClient.accounts[acc] = bal
			go InsertUser(db, acc, bal)
		} else {

			if RpcClient.accounts[acc] != toEth(balance) {
				RpcClient.accounts[acc] = toEth(balance)
				go UpdateBalance(db, acc, toEth(balance))
			}
		}
		RpcClient.mapmutex.Unlock()
	}

}

func handleRequest(conn net.Conn, client *clientRPC) {
	// Make a buffer to hold incoming data.
	buf := make([]byte, 0, 4096) // big buffer
	tmp := make([]byte, 256)     // using small tmo buffer for demonstrating
	for {
		//Read
		n, err := conn.Read(tmp)
		if err != nil {
			//error end of file
			if err != io.EOF {
				fmt.Println("read error:", err)
			}
			//break if end of file
			break
		}
		//fmt.Println("got", n, "bytes.")
		buf = append(buf, tmp[:n]...)

	}
	s := string(buf[:])
	if s[:7] == "GetLast" {
		//GetLast
		lastpostup, err := client.GetLast()
		if err != nil {
			conn.Write([]byte("Error: SendEth " + err.Error()))
			return
		}

		answer, err := json.Marshal(lastpostup)
		if err != nil {
			conn.Write([]byte("Error Marshal answer " + err.Error()))
			return
		}
		conn.Write([]byte(answer))
		return

	} else {
		//SendETH
		s = strings.TrimSpace(s)[strings.Index(s, "(")+1 : strings.Index(s, ")")]
		slice := strings.Split(s, ",")
		if len(slice) > 2 {
			i, err := strconv.ParseFloat(strings.TrimSpace(slice[2]), 64)
			if err != nil {
				conn.Write([]byte("Error third argument must be digits " + err.Error()))
				return
			}
			_, err = client.SendEth(strings.TrimSpace(slice[0]), strings.TrimSpace(slice[1]), toWei(i))
			if err != nil {
				conn.Write([]byte("Error: SendEth " + err.Error()))
				return
			}
			conn.Write([]byte("ETH sended to geth."))
			return
		} else {
			conn.Write([]byte("Error: not enough args received."))
			return
		}

	}

	// Close the connection when you're done with it.
	conn.Close()
}
