package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"strconv"
)

func NewDB(host, port, user, pass, dbname string) *sql.DB {

	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s ",
		host,
		port,
		user,
		pass,
	))
	if err != nil {
		log.Fatalln("create connection to db error %v ", err)
	}

	_, _ = db.Exec("CREATE DATABASE " + dbname)

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS " +
		`accounts(
  			"address" varchar(255) NOT NULL DEFAULT '',
  			"balance" varchar(255) DEFAULT NULL,
  			PRIMARY KEY ("address"))`)
	if err != nil {
		log.Fatalln("create accounts table db error %v ", err)
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS " +
		`transact(
  			"hash" varchar(255) DEFAULT '',
			"nonce" varchar(255) DEFAULT NULL,
  			"blockHash" varchar(255) DEFAULT NULL,
  			"blocknumber" bigint DEFAULT NULL,
			"transactionIndex" bigint DEFAULT NULL,
  			"from" varchar(255) DEFAULT NULL,
  			"to" varchar(255) DEFAULT NULL,
  			"value" text DEFAULT NULL,
			"gasPrice" text DEFAULT NULL,
			"gas" varchar(255) DEFAULT NULL,
  			"input" text,
			"date" varchar(255) ,
			"confirmations" integer)`)
	if err != nil {
		log.Fatalln("create transactions table db error %v ", err)
	}

	return db
}

func InsertTransactions(db *sql.DB, txs []Transaction) error {
	tx, err := db.Begin()
	if err != nil {
		log.Println("tx error %v \n", err)
		return err
	}
	stmt, err := tx.Prepare(`
	INSERT INTO transact(hash, 
	nonce, 
	"blockHash", 
	"blocknumber", 
	"transactionIndex", 
	"from", 
	"to", 
	value, 
	"gasPrice", 
	gas, 
	input, 
	date, 
	confirmations) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)`)
	if err != nil {
		log.Println("stmt err %v \n", err)
	}

	for _, tx := range txs {
		res, err := stmt.Exec(transTx(&tx)...)
		if err != nil {
			log.Println("stmt err %v \n", res, err)
		}
	}
	tx.Commit()
	defer stmt.Close()
	return err

}

func InsertUser(db *sql.DB, addr string, balance float64) error {
	res, err := db.Query("INSERT INTO accounts VALUES('" + addr + "', " + fmt.Sprintf("%.6f", balance) + ")")
	if err != nil {
		log.Println("insert data accounts error: %v\n", err, res)
	}
	return err
}

func SelectUser(db *sql.DB) (map[string]float64, error) {

	tmpmap := make(map[string]float64)
	rows, err := db.Query("SELECT * FROM accounts")
	if err != nil {
		log.Println("insert data accounts error: %v\n", err)
		return nil, err
	}
	tmp := struct {
		address string
		balance float64
	}{}

	for rows.Next() {
		rows.Scan(&tmp.address, &tmp.balance)
		tmpmap[tmp.address] = tmp.balance
	}
	rows.Close()

	return tmpmap, nil
}

func UpdateBalance(db *sql.DB, addr string, balance float64) error {
	bal := strconv.FormatFloat(balance, 'f', 6, 64)
	res, err := db.Query("UPDATE accounts SET balance = " + bal + " WHERE address ='" + addr + "'")
	if err != nil {
		log.Println("insert data accounts error: %v\n", err, res)
	}
	return err
}

func SelectTransaction(db *sql.DB, rpc *clientRPC, lastblock string) ([]Postuplenie, error) {

	tmpme := []Postuplenie{}

	tmp := struct {
		To            string
		Value         string
		Date          string
		Confirmations int
	}{}

	rows, err := db.Query("SELECT \"date\", \"to\", \"value\", \"confirmations\" FROM transact WHERE blocknumber>" + lastblock + " OR confirmations>3")
	if err != nil {
		log.Println("select transaction error: %v\n", err)
		return tmpme, err
	}
	for rows.Next() {
		rows.Scan(&tmp.Date, &tmp.To, &tmp.Value, &tmp.Confirmations)
		s, _ := ParseBigInt(tmp.Value)
		tmpEth := toEth(s)
		if tmpEth > 0 {
			tmpme = append(tmpme,
				Postuplenie{
					tmp.Date,
					tmp.To,
					strconv.FormatFloat(tmpEth, 'f', 6, 64),
					tmp.Confirmations})
		}
	}
	rows.Close()
	rpc.lastBlocknumber = LastBlockNumber(db)
	return tmpme, nil
}

func LastBlockNumber(db *sql.DB) string {
	var lastNumber string
	row := db.QueryRow("select max(transact.blocknumber)  from transact ")
	err := row.Scan(&lastNumber)
	if err != nil {
		lastNumber = "0"
	}
	return lastNumber
}
