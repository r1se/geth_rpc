package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"sync"
)

//struct for response
type clientResponse struct {
	ID      int             `json:"id"`
	JSONRPC string          `json:"jsonrpc"`
	Result  json.RawMessage `json:"result"`
	Error   *ClientError    `json:"error"`
}

//struct for request
type clientRequest struct {
	ID      int           `json:"id"`
	JSONRPC string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
}

// struct for postuplenie
type Postuplenie struct {
	Date          string
	Address       string
	Amount        string
	Confirmations int
}

//error handler struct
type ClientError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

//function for Error interface
func (err ClientError) Error() string {
	return fmt.Sprintf("Error %d (%s)", err.Code, err.Message)
}

/***********************ClientRPC**********************/
// rpc client
type clientRPC struct {
	url             string
	client          *http.Client
	accounts        map[string]float64
	db              *sql.DB
	lastBlocknumber string
	mapmutex        *sync.Mutex
}

// create rpc client with url
func New(db *sql.DB, url string) *clientRPC {
	rpc := &clientRPC{
		url:    url,
		client: http.DefaultClient,
		db:     db,
	}
	var err error
	rpc.accounts, err = SelectUser(db)
	if err != nil {
		log.Fatalln("Error get accounts on constructor: ", err.Error())
	}
	rpc.lastBlocknumber = "-1"
	rpc.mapmutex = &sync.Mutex{}
	return rpc
}

//Call function
func (rpc *clientRPC) Call(method string, params ...interface{}) (json.RawMessage, error) {
	request := clientRequest{
		ID:      1,
		JSONRPC: "2.0",
		Method:  method,
		Params:  params,
	}

	body, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	response, err := rpc.client.Post(rpc.url, "application/json", bytes.NewBuffer(body))
	if response != nil {
		defer response.Body.Close()
	}
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	resp := new(clientResponse)
	if err := json.Unmarshal(data, resp); err != nil {
		return nil, err
	}

	if resp.Error != nil {
		return nil, *resp.Error
	}

	return resp.Result, nil

}

//implementations for many Call and unmarshall
func (rpc *clientRPC) call(method string, target interface{}, params ...interface{}) error {
	result, err := rpc.Call(method, params...)
	if err != nil {
		return err
	}

	if target == nil {
		return nil
	}

	return json.Unmarshal(result, target)
}

//Send ethereum
func (rpc *clientRPC) SendEth(From string, To string, Amount *big.Int) (string, error) {
	var hash string
	err := rpc.call("eth_sendTransaction", &hash, T{From: From, To: To, Value: Amount})
	return hash, err
}

//Get Last postuplenya
func (rpc *clientRPC) GetLast() ([]Postuplenie, error) {
	return SelectTransaction(rpc.db, rpc, rpc.lastBlocknumber)
}

//Get all accounts
func (rpc *clientRPC) Accounts() ([]string, error) {
	accounts := []string{}

	err := rpc.call("eth_accounts", &accounts)
	return accounts, err
}

//Get balance for accounts
func (rpc *clientRPC) GetBalance(address, block string) (big.Int, error) {
	var response string
	if err := rpc.call("eth_getBalance", &response, address, block); err != nil {
		return big.Int{}, err
	}

	return ParseBigInt(response)
}

//GetBlockNumber
func (rpc *clientRPC) GetBlockNumber() (int, error) {
	var response string
	if err := rpc.call("eth_blockNumber", &response); err != nil {
		return 0, err
	}

	return ParseInt(response)
}

// returns the information about a transaction
func (rpc *clientRPC) GetTransactionByHash(hash string) (*Transaction, error) {

	transaction := new(Transaction)
	err := rpc.call("eth_getTransactionByHash", &transaction, hash)
	return transaction, err
}

func (rpc *clientRPC) GetFilterChanges(filterID string) ([]string, error) {
	var logs []string
	err := rpc.call("eth_getFilterChanges", &logs, filterID)
	if err != nil {
		log.Println("Error logs filter: " + err.Error())
	}
	return logs, err
}

func (rpc *clientRPC) NewBlockFilter() (string, error) {
	var filterID string
	err := rpc.call("eth_newBlockFilter", &filterID)
	return filterID, err
}

func (rpc *clientRPC) NewPendingTransactionFilter() (string, error) {
	var filterID string
	err := rpc.call("eth_newPendingTransactionFilter", &filterID)
	return filterID, err
}

func (rpc *clientRPC) getBlock(method string, withTransactions bool, params ...interface{}) (*Block, error) {
	var response proxyBlock
	if withTransactions {
		response = new(proxyBlockWithTransactions)
	} else {
		response = new(proxyBlockWithoutTransactions)
	}

	err := rpc.call(method, response, params...)
	if err != nil {
		return nil, err
	}
	block := response.toBlock()

	return &block, nil
}

func (rpc *clientRPC) GetBlockByHash(hash string, withTransactions bool) (*Block, error) {
	return rpc.getBlock("eth_getBlockByHash", withTransactions, hash, withTransactions)
}
